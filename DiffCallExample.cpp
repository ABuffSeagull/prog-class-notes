// Call By Value
void
    swap(int a, int b)
{
    int temp = a
    a = b;
    b = temp;
}
void main()
{
    int a = 2;
    int b = 3;
    
    swap(a, b);
}


// Call By Reference
void swap(int* ints,
    int i_1, int i_2)
{
    int temp =
        ints[i_1];
    ints[i_1] =
        ints[i_2];
    ints[i_2] = temp;
}

public void main()
{
    int[] ints =
        {1, 2, 3, 4};
    
    swap(ints, 0, 3);
}

// Call By Reference 2
void swap(int &a,
    int &b)
{
    int temp = a;
    a = b;
    b = temp;
}
void main()
{
    int a = 2;
    int b = 3;
    
    swap(a, b);
}