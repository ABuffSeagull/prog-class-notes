public class Person
{
    private:
        const string name;
        int age;
    
    public:
        Person(string name, int age)
        string getName();
        int getAge();
        void haveABirthday();
}

string Person::getName()
{
    return this->name;
}

int Person::getName()
{
    return this->age;
}

public void haveABirthday()
{
    this->age++;
}

// Error Handling

Person::Person(string name, int age)
{
    if (name.compare("") == 0)
    {
        throw invalid_argument("Name may not be null!");
    }
    if (age < 0)
    {
        throw out_of_range("Age must be non-negative!");
    }
}


// Now using const!

public class Person
{
    private:
        const string name;
        int age;
    
    public:
        Person(string name, int age)
        string getName() const;
        int getAge() const;
        void haveABirthday();
}

string Person::getName() const
{
    return this->name;
}

int Person::getName() const
{
    return this->age;
}

public void haveABirthday()
// Cannot declare as const, because it would treat age as const
// this would give a compile-time error
{
    this->age++;
}